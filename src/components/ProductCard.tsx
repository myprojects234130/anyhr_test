import React from "react";
import { Card, CardContent, Typography, Button } from "@mui/material";
import Link from "next/link";

interface ProductCardProps {
  product: {
    id: number;
    name: string;
    description: string;
    price: number;
  };
}

const ProductCard = ({ product }: ProductCardProps) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {product.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {product.description}
        </Typography>
        <Typography variant="h6">${product.price}</Typography>
        <Link href={`/product/${product.id}`} passHref>
          <Button variant="contained" color="primary">
            View Details
          </Button>
        </Link>
      </CardContent>
    </Card>
  );
};

export default ProductCard;
