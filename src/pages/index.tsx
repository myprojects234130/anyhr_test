import { Box, Button, Container, Typography } from "@mui/material";
import { useRouter } from "next/router";
import { StyledContainer } from "../styles";

const HomePage = () => {
  const router = useRouter();

  const navigateToCatalog = () => {
    router.push("/catalog?page=1&limit=5");
  };

  return (
    <StyledContainer>
      <Typography variant="h1" gutterBottom>
        Welcome to Our Website
      </Typography>
      <Box marginBottom={2}>
        <Button variant="contained" color="primary" onClick={navigateToCatalog}>
          Go to Catalog
        </Button>
      </Box>
    </StyledContainer>
  );
};

export default HomePage;
