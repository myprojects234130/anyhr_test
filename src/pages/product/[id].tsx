import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import {
  Box,
  Container,
  Typography,
  Button,
  CircularProgress,
} from "@mui/material";
import { products } from "../../mock/data";
import { StyledContainer } from "../../styles";

interface ProductProps {
  product: {
    id: number;
    name: string;
    description: string;
    price: number;
  };
}

const ProductPage = ({ product }: ProductProps) => {
  const router = useRouter();

  if (router.isFallback) {
    return (
      <Container>
        <Head>
          <title>Loading...</title>
          <meta name="description" content="Loading product details..." />
        </Head>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          height="100vh"
        >
          <CircularProgress />
        </Box>
      </Container>
    );
  }

  if (!product) {
    return (
      <StyledContainer>
        <Head>
          <title>Product Not Found</title>
          <meta name="description" content="Product not found in catalog." />
        </Head>
        <Typography variant="h6" gutterBottom>
          Product not found
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => router.back()}
        >
          Back to Catalog
        </Button>
      </StyledContainer>
    );
  }

  return (
    <StyledContainer>
      <Head>
        <title>{product.name}</title>
        <meta name="description" content={product.description} />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org/",
              "@type": "Product",
              name: product.name,
              description: product.description,
              sku: product.id.toString(),
              offers: {
                "@type": "Offer",
                priceCurrency: "USD",
                price: product.price,
              },
            }),
          }}
        />
      </Head>
      <Typography variant="h1" gutterBottom>
        {product.name}
      </Typography>
      <Typography variant="body1" gutterBottom>
        {product.description}
      </Typography>
      <Typography variant="h6" gutterBottom>
        Price: ${product.price}
      </Typography>
      <Button variant="contained" color="primary" onClick={() => router.back()}>
        Back to Catalog
      </Button>
    </StyledContainer>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = products.map((product) => ({
    params: { id: product.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { id } = context.params as { id: string };
  const product = products.find((p) => p.id === parseInt(id));

  return {
    props: {
      product: product || null,
    },
    revalidate: 10,
  };
};

export default ProductPage;
