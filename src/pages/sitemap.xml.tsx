import { NextApiResponse } from "next";
import { products } from "../mock/data";

const Sitemap = () => {};

export const getServerSideProps = async ({ res }: { res: NextApiResponse }) => {
  const productsPaths = products.map((product) => `/product/${product.id}`);

  const staticPages = ["/catalog"];

  const pages = [...staticPages, ...productsPaths];

  const xml = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${pages
        .map((page) => {
          return `
            <url>
              <loc>${`https://yourdomain.com${page}`}</loc>
            </url>
          `;
        })
        .join("")}
    </urlset>
  `;

  res.setHeader("Content-Type", "text/xml");
  res.write(xml);
  res.end();

  return { props: {} };
};

export default Sitemap;
