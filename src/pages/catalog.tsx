import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { Box, Container, Grid, Pagination, Typography } from "@mui/material";
import ProductCard from "../components/ProductCard";
import { products } from "../mock/data";
import Head from "next/head";
import { StyledContainer } from "../styles";

interface CatalogProps {
  products: typeof products;
  total: number;
  page: number;
  limit: number;
}

const CatalogPage = ({ products, total, page, limit }: CatalogProps) => {
  const router = useRouter();

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    router.push(`/catalog?page=${value}&limit=${limit}`);
  };

  return (
    <StyledContainer>
      <Head>
        <title>Product Catalog - Page {page}</title>
        <meta name="description" content="Browse our products catalog." />
      </Head>
      <Typography variant="h1" gutterBottom>
        Product Catalog
      </Typography>
      <Grid container spacing={2}>
        {products.map((product) => (
          <Grid item xs={12} sm={6} md={4} key={product.id}>
            <ProductCard product={product} />
          </Grid>
        ))}
      </Grid>
      <Box display="flex" justifyContent="center" marginTop={2}>
        <Pagination
          count={Math.ceil(total / limit)}
          page={page}
          onChange={handlePageChange}
          color="primary"
        />
      </Box>
    </StyledContainer>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { page = "1", limit = "5" } = context.query;

  const pageNumber = parseInt(page as string);
  const limitNumber = parseInt(limit as string);
  const start = (pageNumber - 1) * limitNumber;
  const end = start + limitNumber;

  const paginatedProducts = products.slice(start, end);

  return {
    props: {
      products: paginatedProducts,
      total: products.length,
      page: pageNumber,
      limit: limitNumber,
    },
  };
};

export default CatalogPage;
