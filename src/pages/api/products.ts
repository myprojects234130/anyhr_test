import { NextApiRequest, NextApiResponse } from "next";
import { products } from "../../mock/data";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { page = 1, limit = 5 } = req.query;

  const pageNumber = parseInt(page as string);
  const limitNumber = parseInt(limit as string);
  const start = (pageNumber - 1) * limitNumber;
  const end = start + limitNumber;

  const paginatedProducts = products.slice(start, end);

  res.status(200).json({
    page: pageNumber,
    limit: limitNumber,
    total: products.length,
    data: paginatedProducts,
  });
}
