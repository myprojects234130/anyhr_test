import { styled } from "@mui/system";

export const StyledContainer = styled("div")({
  display: "inline-block",
  width: "auto",
  margin: "0 auto",
  padding: "20px",
  boxShadow: "0 0 10px rgba(0, 0, 0, 0.1)",
  borderRadius: "8px",
  backgroundColor: "#f9f9f9",
});
